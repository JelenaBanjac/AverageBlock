--------------------------------------------------------------------------------
--
--   FileName:         average.vhd
--   Dependencies:     none
--   Design Software:  Quartus II 64-bit Version 13.1 Full Version
-- 
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
--use work.global.all;
use work.fixed_point_adder;
use work.qdiv;

entity average is
	generic (N : natural := 48);
	port (
		-- Clock.
		a_clk				: in std_logic;
		-- Reset.
		a_rst				: in std_logic;
		
		-- Input port.
		data_valid			: in std_logic;
		data_in_X			: in std_logic_vector(N-1 downto 2*N/3);
		data_in_Y			: in std_logic_vector(2*N/3-1 downto N/3);
		data_in_Z			: in std_logic_vector(N/3-1 downto 0);
		
		-- Output port.
		data_valid_cnf			: out std_logic;
		data_out			: out std_logic_vector(N-1 downto 0)
	);
end entity average;

architecture arch_average of average is
	-- Clock.
	signal clk					: std_logic;
	-- Reset.
	signal rst					: std_logic;
	
	-- Type of states in state machine
	type state_type is (restart,s0,s1,s2,s3);  	
	-- current and next state declaration	
	signal current_s, next_s			: state_type;  	
	
	-- Data for fixed-point addition (3)
	signal r1, r2, r3, R_No				: std_logic_vector(N-1 downto 0);
	signal r1_copy, r2_copy, r3_copy 		: std_logic_vector(N-1 downto 0);
	signal r1_tmp, r2_tmp, r3_tmp	 		: std_logic_vector(N-1 downto 0);
	
	signal data, result, division_result		: std_logic_vector(N-1 downto 0);
	
	-- Division signals.
	signal division_complete, division_overflow 	: std_logic;
	-- Data valid signals.
	signal dv, dv1, dv2, dv3			: std_logic;
begin
	R_No <= "000000000000000000000000000000000000000000000011";
	clk <= a_clk;
	rst <= a_rst;

	---
	data(N-1 downto 2*N/3) <= data_in_X;	--highest bits are for X
	data(2*N/3-1 downto N/3) <= data_in_Y;
	data(N/3-1 downto 0) <= data_in_Z;	--lowest bits are for Z
	---
	
	

	dv1 <= data_valid; 

	

	process (clk, rst)
	begin
		if (rst='0') then
			r1	<= (others => '0');	
			r2	<= (others => '0');	
			r3	<= (others => '0');
			--dv1 	<= '0';

		elsif (rising_edge(clk)) then
			r1 <= data;
			r2 <= r1_copy;
			r3 <= r2_copy;
			--dv1 	<= '1';	
		
		end if;
	end process;

	process (clk, rst)
	begin
		if (rst='0') then
			r1_copy <= (others => '0');
			r2_copy <= (others => '0');
			r3_copy <= (others => '0');
		elsif (rising_edge(clk)) then
			r1_copy <= r1;
			r2_copy <= r2;
			r3_copy <= r3;
		end if;
	end process;

	
	process (clk, rst)
	begin
		if (rst='0') then
			dv2 <= '0';
                        r2_tmp <= (others => '0');
                        r3_tmp <= (others => '0'); 

		elsif (rising_edge(clk)) then
			dv2 <= dv1;	--propagate data value
			r2_tmp <= r1_tmp;
                        r3_tmp <= r3;
		end if;
	end process;

	process (clk, rst)
	begin
		if (rst='0') then
			data_out <= (others => '0');
		elsif (rising_edge(clk)) then
			data_valid_cnf <= dv2;
			--data_valid_cnf <= division_complete;
			--data_out <= division_result; 
			data_out <= result; 
		end if;
	end process;
	
	--data_valid_cnf <= dv3;


	---------------------------------------------------
	--- 		X Fixed-Point Adder		---
	---------------------------------------------------
	--1st pair: fixed-point ADDITION
	fixed_point_adder_x_1: entity fixed_point_adder
	generic map (
		N => N/3
	)
	port map(
		i_clk  	=> clk,
		i_rst 	=> rst,
		
		i_A	=> r1(N-1 downto 2*N/3),
		i_B	=> r2(N-1 downto 2*N/3),
		
		o_C    	=> r1_tmp(N-1 downto 2*N/3)
	);
	
	--2nd pair: fixed-point ADDITION
	fixed_point_adder_x_2: entity fixed_point_adder
	generic map (
		N => N/3
	)
	port map(
		i_clk  	=> clk,
		i_rst 	=> rst,
		
		i_A	=> r2_tmp(N-1 downto 2*N/3),
		i_B	=> r3_tmp(N-1 downto 2*N/3),
		
		o_C    	=> result(N-1 downto 2*N/3)
	);

	---------------------------------------------------
	--- 		Y Fixed-Point Adder		---
	---------------------------------------------------
	--1st pair: fixed-point ADDITION
	fixed_point_adder_y_1: entity fixed_point_adder
	generic map (
		N => N/3
	)
	port map(
		i_clk  	=> clk,
		i_rst 	=> rst,
		
		i_A	=> r1(2*N/3-1 downto N/3),
		i_B	=> r2(2*N/3-1 downto N/3),
		
		o_C    	=> r1_tmp(2*N/3-1 downto N/3)
	);
	
	--2nd pair: fixed-point ADDITION
	fixed_point_adder_y_2: entity fixed_point_adder
	generic map (
		N => N/3
	)
	port map(
		i_clk  	=> clk,
		i_rst 	=> rst,
		
		i_A	=> r2_tmp(2*N/3-1 downto N/3),
		i_B	=> r3_tmp(2*N/3-1 downto N/3),
		
		o_C    	=> result(2*N/3-1 downto N/3)
	);

	---------------------------------------------------
	--- 		Z Fixed-Point Adder		---
	---------------------------------------------------
	--1st pair: fixed-point ADDITION
	fixed_point_adder_z_1: entity fixed_point_adder
	generic map (
		N => N/3
	)
	port map(
		i_clk  	=> clk,
		i_rst 	=> rst,
		
		i_A	=> r1(N/3-1 downto 0),
		i_B	=> r2(N/3-1 downto 0),
		
		o_C    	=> r1_tmp(N/3-1 downto 0)
	);
	
	--2nd pair: fixed-point ADDITION
	fixed_point_adder_z_2: entity fixed_point_adder
	generic map (
		N => N/3
	)
	port map(
		i_clk  	=> clk,
		i_rst 	=> rst,
		
		i_A	=> r2_tmp(N/3-1 downto 0),
		i_B	=> r3_tmp(N/3-1 downto 0),
		
		o_C    	=> result(N/3-1 downto 0)
	);
	
	---------------------------------------------------
	--- 		Fixed-Point Division		---
	---------------------------------------------------
	fixed_point_divider: entity qdiv
	port map(
		i_clk  	=> clk,
		i_start => rst,
		
		i_dividend => result,
		i_divisor => R_No,
		
		o_quotient_out => division_result,
		o_complete => division_complete,
		o_overflow => division_overflow
	);
	
	
end arch_average;
