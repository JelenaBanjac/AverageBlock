--------------------------------------------------------------------------------
--
--   FileName:         average_tb.vhd
--	  Description:		  Testbench for the average module.
--   Dependencies:     none
--   Design Software:  Quartus II 64-bit Version 13.1 Full Version
-- 
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use work.average;

entity average_tb is
end entity average_tb;

architecture arch_average_tb of average_tb is
	-- How many bits do we have?
	constant N : integer := 60;
	
	-- Component.
	component average
	port (
		a_clk			:	in std_logic;
		a_rst			:	in std_logic;
		data_valid		:	in std_logic;

		data_in_X		:	in std_logic_vector(N/3-1 downto 0);
		data_in_Y		:	in std_logic_vector(N/3-1 downto 0);
		data_in_Z		:	in std_logic_vector(N/3-1 downto 0);
		
		available		:	out std_logic;		
		data_valid_cnf_addition	:	out std_logic;		
		data_valid_cnf		:	out std_logic;
		data_out_addition	:	out std_logic_vector(N-1 downto 0);
		data_out		:	out std_logic_vector(N-1 downto 0)
	);
	end component;

	-- For the println :)
	file stdout: text open write_mode is "STD_OUTPUT";
	procedure println(s: string) is
		variable l: line;
	begin
		write(l, s);
		writeline(stdout, l);
	end procedure println; 

	-- Clock.
	signal a_clk   			: std_logic := '0';
	-- Reset.
	signal a_rst    		: std_logic := '0';
   
	-- Inputs.
	signal data_valid    		: std_logic := '0';
   	

	signal data_in_X   		: std_logic_vector(N/3-1 downto 0) := (others => '0');
	signal data_in_Y   		: std_logic_vector(N/3-1 downto 0) := (others => '0');
	signal data_in_Z   		: std_logic_vector(N/3-1 downto 0) := (others => '0');
	
	-- Outputs.
	signal available    		: std_logic := '0';	
	signal data_valid_cnf_addition  : std_logic;
  	signal data_valid_cnf   	: std_logic;
   	signal data_out 		: std_logic_vector(N-1 downto 0);
	signal data_out_addition 	: std_logic_vector(N-1 downto 0);

	-- Clock period definitions
	constant a_clk_period 		: time := 20 ns; -- 50MHz

	--very important this signal should be an integer
	signal count 			: integer range 0 to 127;
	signal it    			: std_logic_vector (127 downto 0);

begin

	-- Instantiate the Unit Under Test (UUT)
	uut: average
	port map (
		a_clk		=> a_clk,
		a_rst 		=> a_rst,
		available 	=> available,
		data_valid 	=> data_valid,
		data_in_X	=> data_in_X,
		data_in_Y	=> data_in_Y,
		data_in_Z	=> data_in_Z,
		data_valid_cnf_addition  => data_valid_cnf_addition,
		data_out	=> data_out,
		data_out_addition	=> data_out_addition,
		data_valid_cnf  => data_valid_cnf
	);
	
	-- Clock process definitions
	a_clk_process :process
	begin
		a_clk <= '0';
		wait for a_clk_period/2;	--for 10ns signal is '0'
		a_clk <= '1';
		wait for a_clk_period/2;	--for next 10ns signal is '1'
	end process;
	
	-- Simulating Z data input
	count_tb: process (a_clk,a_rst)
	begin
	  if (a_rst = '0') then
		data_in_Z <= (others => '0');
	  elsif (rising_edge(a_clk)) then
	    data_in_Z <= std_logic_vector( unsigned(data_in_Z) + 1 );    
	  end if;
	end process;

	-- Data valid signal goes up every second clock cycle
	data_valid_tb: process (a_clk,a_rst)
	begin
	  if (a_rst = '0') then
		data_valid <= '0';
	  elsif (rising_edge(a_clk)) then
		if (data_valid='0') then 
			data_valid <= '1';
		else 
			data_valid <= '0';
		end if;
	  end if;
	end process;


	tb: process
	begin
		wait for a_clk_period/2; -- Wait a bit in a begining (40ns).
		
      		-- Implementation of the testbench.
		a_rst <= '1';
		
		println("--------------------------------------");
		println("Testbench done!");
		println("--------------------------------------");
		
		wait; -- Wait forever.
	end process;

end architecture arch_average_tb;
