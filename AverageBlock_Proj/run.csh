#!/bin/tcsh
setenv LM_LICENSE_FILE 1800@lxlicen01,1800@lxlicen02,1800@lxlicen03:/home/cfaerber/qpifpga/SR-4.1.7/Base/HW/qpi-qxp/license.dat
setenv MGLS_LICENSE_FILE 1717@lxlicen01,1717@lxlicen02,1717@lxlicen03,1717@lnxmics1,1717@lxlicen08
setenv CDS_LIC_FILE 2000@lxlicen01,2000@lxlicen02,2000@lxlicen03

	vlib lpm_ver
	vlib altera_mf_ver
	vmap lpm_ver lpm_ver
	vmap altera_mf_ver altera_mf_ver
	vlog -work altera_mf_ver /opt/altera/13.1/quartus/eda/sim_lib/altera_mf.v
	vlog -work lpm_ver /opt/altera/13.1/quartus/eda/sim_lib/220model.v

#r/m -r -f work

#vlib work
#vmap work work
#vcom average_tb.vhd
vcom work/fixed_point_adder.vhd
vlog work/qdiv.v
vcom work/average.vhd
vcom average_tb.vhd
vmap work work


#vsim average_tb

vsim -do average.do average_tb
