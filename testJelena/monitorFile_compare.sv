module monitorFile_compare (
  clk,
  enable,
  mode,
  dataToMon,
  dataCompare
  );

  input wire clk;
  input wire enable;
  input wire mode;
  input wire [31:0] dataToMon;
  input wire [31:0] dataCompare;
  
  
  integer f,i;

  //Clock and reset release
  initial begin

  end

  initial begin
  

	f = $fopen("compare.txt","w");  

    @(posedge enable); //Wait for enable
    @(posedge clk);

    while (1'b1) begin
      @(posedge clk);
		
	  if(enable) begin
      $fwrite(f,"%f\n", $bitstoshortreal(dataToMon));
	  end
    end

    $fclose(f);  

    $finish;
  end
  
endmodule