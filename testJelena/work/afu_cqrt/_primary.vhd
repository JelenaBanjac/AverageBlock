library verilog;
use verilog.vl_types.all;
entity afu_cqrt is
    generic(
        widthSort       : integer := 32;
        cqrt_pipe       : integer := 25
    );
    port(
        clk             : in     vl_logic;
        reset_n         : in     vl_logic;
        spl_reset       : in     vl_logic;
        data_in         : in     vl_logic_vector;
        newData         : in     vl_logic;
        data_out        : out    vl_logic_vector;
        tx_data_rich_valid: out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of widthSort : constant is 1;
    attribute mti_svvh_generic_type of cqrt_pipe : constant is 1;
end afu_cqrt;
