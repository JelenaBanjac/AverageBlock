//--------------------------------------------
// afu_RICH
// Author: Christian Faerber
//--------------------------------------------

integer i,j;

module afu_cqrt #(parameter widthSort=32, parameter cqrt_pipe=10+15)
	(
      input wire                  	clk,
      input wire                  	reset_n, 
	  input wire					spl_reset,
		
	  // Input
	  input wire [widthSort-1:0]	data_in,
	  input wire 					newData,
		
	  // Output
	  output reg [widthSort-1:0]	data_out,
	  output reg 					tx_data_rich_valid
	);

	
	// Fetch data 
	reg [widthSort-1:0]				fetchreg;
	reg 							fetchnewData;
	// Modulo exponent
	reg [8+10-1:0]					calcreg;
	reg [8+4-1:0]					calcreg2;
	reg [2:0]						modulo;
	reg [7:0]						exponent_div;
	reg [7:0]						exponent_div_tmp;
	reg [22:0]						mantisse_p [2:0];
	reg 							sign_p [3:0];
	reg 							dataValid_p [3:0];
	reg								belowOne,belowOne_p,belowOne_p2;
	reg								isOne,isOne_p;
	reg								betweenOneTwo,betweenOneTwo_p,betweenOneTwo_p2;
	// Prepare mantisse 
	reg [7:0]						exponent_new;
	reg [26:0]						mantisse_prepared;
	reg [2:0]						modulo_p;
	// Cubic root 
	reg [26+3*15:0]					mantisse_p2 [cqrt_pipe-1:0];
	reg [26+3*15:0]					mantisse_p2_tmp [cqrt_pipe-1:0];
//	reg [1:0]						modulo_p2 [cqrt_pipe-1:0];
	reg [cqrt_pipe-1:0]				mantisse_result_p [cqrt_pipe-1:0];
	reg [cqrt_pipe-1:0]				mantisse_result_p_tmp [cqrt_pipe-1:0];	
//	reg [26+3*15:0]					result_sub [cqrt_pipe-1:0];
//	reg [26+3*15:0]					result_sub_tmp [cqrt_pipe-1:0];
//	reg [26+3*15:0]					calcMult [cqrt_pipe-1:0];
	
	reg [26+15:0]					result_sub [cqrt_pipe-1:0];
	reg [26+15:0]					result_sub_tmp [cqrt_pipe-1:0];
	reg [26+15:0]					calcMult [cqrt_pipe-1:0];
	   //
	reg [7:0]						exponent_new_p [2*cqrt_pipe-1:0];
	reg 							sign_p2 [2*cqrt_pipe-1:0];
	reg 							dataValid_p2 [2*cqrt_pipe-1:0];
	// Trans data 


	
	//////////////////////
	// Fetch data 
	//////////////////////
	always @(posedge clk) begin
        if ((~reset_n) | spl_reset) begin
			fetchnewData		<=  1'b0;
			fetchreg			<= 32'b0;
        end
        
        else begin
			fetchnewData		<= newData;
			fetchreg			<= data_in;	
		end
	end
	
	
	
	//////////////////////
	// Modulo exponent
	//////////////////////
	always @(posedge clk) begin
        if ((~reset_n) | spl_reset) begin
			calcreg						<= 18'b0;
			calcreg2					<= 12'b0;	
			modulo 						<=  3'b0;
			exponent_div_tmp			<=  8'b0;
			exponent_div				<=  8'b0;
			mantisse_p[0]				<= 23'b0;
			mantisse_p[1]				<= 23'b0;
			mantisse_p[2]				<= 23'b0;
			sign_p[0]					<=  1'b0;
			sign_p[1]					<=  1'b0;
			sign_p[2]					<=  1'b0;
			dataValid_p[0]				<=  1'b0;
			dataValid_p[1]				<=  1'b0;
			dataValid_p[2]				<=  1'b0;
			belowOne					<=  1'b0;
			belowOne_p					<=  1'b0;
			belowOne_p2					<=  1'b0;
			isOne						<=  1'b0;
			isOne_p						<=  1'b0;
			betweenOneTwo				<=  1'b0;
			betweenOneTwo_p				<=  1'b0;
			betweenOneTwo_p2			<=  1'b0;
        end
        
        else begin
		
			// Check between One and Two
			if({1'b0,fetchreg[30:0]}>32'h3F800000 && {1'b0,fetchreg[30:0]}<32'h40000000) begin
				betweenOneTwo			<=  1'b1;
			end
			else begin
				betweenOneTwo			<=  1'b0;
			end
			
			betweenOneTwo_p				<= betweenOneTwo;
			betweenOneTwo_p2			<= betweenOneTwo_p;
			
		
			if(fetchreg[30:23]<127) begin
				calcreg					<=(127-fetchreg[30:23])*341;
				belowOne				<= 1'b1;
			end
			else begin
				calcreg					<= (fetchreg[30:23]-127)*341; //fetchreg[30:23]*341;
				belowOne				<= 1'b0;
			end
			
			calcreg2					<= calcreg[9:0]*3;
			
			if(belowOne == 1'b0) begin
				exponent_div_tmp		<= calcreg[17:10]+127;
			end		
			else begin
				exponent_div_tmp		<= 127-calcreg[17:10]-1;
			end
			
			belowOne_p					<= belowOne;
			belowOne_p2					<= belowOne_p;
			
			exponent_div				<= exponent_div_tmp;
			if(belowOne_p==1'b0) begin
				modulo					<= {1'b0,calcreg2[11:10]};
			end
			else begin
				modulo					<= {1'b0,calcreg2[11:10]+1};
			end
			
			
			// Check if One
			if(fetchreg[30:23]==8'h7F && fetchreg[22:0]==0) begin
				isOne					<= 1'b1;
			end
			else begin
				isOne					<= 1'b0;
			end
			isOne_p						<= isOne;

			if(isOne_p==1'b1) begin
				modulo[2]				<= 1'b1;
			end
			//

			mantisse_p[0]				<= fetchreg[22:0];
			mantisse_p[1]				<= mantisse_p[0];
			mantisse_p[2]				<= mantisse_p[1];
			sign_p[0]					<= fetchreg[31];
			sign_p[1]					<= sign_p[0];
			sign_p[2]					<= sign_p[1];
			dataValid_p[0]				<= fetchnewData;
			dataValid_p[1]				<= dataValid_p[0];
			dataValid_p[2]				<= dataValid_p[1];
		end
	end
	
	
	
	//////////////////////
	// Prepare mantisse 
	//////////////////////
	always @(posedge clk) begin
        if ((~reset_n) | spl_reset) begin
			exponent_new				<=   8'b0;
			mantisse_prepared			<= 	27'b0;
			modulo_p					<=   3'b0;
			sign_p[3]					<=   1'b0;
			dataValid_p[3]				<=   1'b0;
        end
        
        else begin
			if(modulo[1:0] == 2'b10 && belowOne_p2 == 1'b0) begin
				exponent_new				<=  exponent_div + 1;			
			end
			else begin
				exponent_new				<=  exponent_div;						
			end
			
			modulo_p						<=  modulo;
			
			case ({modulo,belowOne_p2,betweenOneTwo_p2})
				// higher one larger two
				////////////////////////
				5'b00000 : mantisse_prepared			<= 	{1'b0, 1'b1, mantisse_p[2], 2'b0};
				
				5'b00100 : mantisse_prepared			<= 	{1'b1, mantisse_p[2], 3'b0};
				
				5'b01000 : mantisse_prepared			<= 	{2'b0, 1'b1, mantisse_p[2], 1'b0};
				
				//Case x
				5'b01100 : mantisse_prepared			<= 	{2'b0, 1'b1, mantisse_p[2], 1'b0};
				
				
				// higher one smaller two
				/////////////////////////
				5'b00001 : mantisse_prepared			<= 	{2'b0, 1'b1, mantisse_p[2], 1'b0};
				
				5'b00101 : mantisse_prepared			<= 	{1'b1, mantisse_p[2], 3'b0};
				
				5'b01001 : mantisse_prepared			<= 	{2'b0, 1'b1, mantisse_p[2], 1'b0};
				
				//Case x
				5'b01101 : mantisse_prepared			<= 	{2'b0, 1'b1, mantisse_p[2], 1'b0};
				
				
				// below one
				////////////
				5'b00010 : mantisse_prepared			<= 	{1'b0, 1'b1, mantisse_p[2], 2'b0};
				
				5'b00110 : mantisse_prepared			<= 	{1'b1, mantisse_p[2], 3'b0};
				
				5'b01010 : mantisse_prepared			<= 	{1'b0, 1'b1, mantisse_p[2], 2'b0};
				
				//Case x
				5'b01110 : mantisse_prepared			<= 	{2'b0, 1'b1, mantisse_p[2], 1'b0};
				

				//Case 1.00000
				//////////////
				5'b1xxxx : mantisse_prepared			<= 	{1'b0, mantisse_p[2], 3'b0};
				
				default : mantisse_prepared			<= 	{2'b0, 1'b1, mantisse_p[2], 1'b0};
			endcase
			
			sign_p[3]						<=  sign_p[2];
			dataValid_p[3]					<=  dataValid_p[2];
		end
	end
	
	
/*		
	////////////////////////
	// cubic root - 9-Steps
	////////////////////////
	always @(posedge clk) begin
        if ((~reset_n) | spl_reset) begin
		
			for(i=0;i<cqrt_pipe;i++) begin
				mantisse_p2[i]				<= 27'b0;
				//modulo_p2[i]				<=  2'b0;
				mantisse_result_p[i]		<=  9'b0;
				result_sub[i]				<= 27'b0;
				//
				exponent_new_p[i]			<=  8'b0;
				sign_p2[i]					<=  1'b0;
				dataValid_p2[i]				<=  1'b0;
			end		

        end
        
        else begin
		
			mantisse_p2[0]					<= mantisse_prepared;
			//modulo_p2[0]					<= modulo_p;
			mantisse_result_p[0]			<=  9'b0; 
			result_sub[0]					<= 24'b0;
			//
			exponent_new_p[0]				<= exponent_new;
			sign_p2[0]						<= sign_p[3];
			dataValid_p2[0]					<= dataValid_p[3];
			for(i=1;i<cqrt_pipe;i++) begin
				mantisse_p2[i]				<= mantisse_p2[i-1];
				//modulo_p2[i]				<= modulo_p2[i-1];
				mantisse_result_p[i]		<= mantisse_result_p[i-1];
				//result_sub[i]				<= result_sub[i-1];
				//
				exponent_new_p[i]			<= exponent_new_p[i-1];
				sign_p2[i]					<= sign_p2[i-1];
				dataValid_p2[i]				<= dataValid_p2[i-1];
			end
			
			
			//Step1
			if(mantisse_p2[0][26:24] >= 1) begin
				mantisse_result_p[1][8]		<= 1'b1;
				result_sub[1]				<= {21'b0, mantisse_p2[0][26:24]-1, mantisse_p2[0][23:21]};
			end
			
			else begin
				mantisse_result_p[1][8]		<= 1'b0;
				result_sub[1]				<= {21'b0, mantisse_p2[0][26:24], mantisse_p2[0][23:21]};
			end
			
			//Step2
			if(result_sub[1] >= (3*{mantisse_result_p[1][8],1'b0}*(1+{mantisse_result_p[1][8],1'b0})+1'b1)) begin
				mantisse_result_p[2][7]		<= 1'b1;
				result_sub[2]				<= { (result_sub[1]-(3*{mantisse_result_p[1][8],1'b0}*(1+{mantisse_result_p[1][8],1'b0})+1'b1)), mantisse_p2[1][20:18]};
			end
			
			else begin
				mantisse_result_p[2][7]		<= 1'b0;
				result_sub[2]				<= {(result_sub[1]), mantisse_p2[1][20:18]};
			end
			
			//Step3
			if(result_sub[2] >= (3*{mantisse_result_p[2][8:7],1'b0}*(1+{mantisse_result_p[2][8:7],1'b0})+1'b1)) begin
				mantisse_result_p[3][6]		<= 1'b1;
				result_sub[3]				<= { result_sub[2]-(3*{mantisse_result_p[2][8:7],1'b0}*(1+{mantisse_result_p[2][8:7],1'b0})+1'b1), mantisse_p2[2][17:15]};
			end
			
			else begin
				mantisse_result_p[3][6]		<= 1'b0;
				result_sub[3]				<= {(result_sub[2]), mantisse_p2[2][17:15]};
			end
			
			//Step4
			if(result_sub[3] >= (3*{mantisse_result_p[3][8:6],1'b0}*(1+{mantisse_result_p[3][8:6],1'b0})+1'b1)) begin
				mantisse_result_p[4][5]		<= 1'b1;
				result_sub[4]				<= { result_sub[3]-(3*{mantisse_result_p[3][8:6],1'b0}*(1+{mantisse_result_p[3][8:6],1'b0})+1'b1), mantisse_p2[3][14:12]};
			end
			
			else begin
				mantisse_result_p[4][5]		<= 1'b0;
				result_sub[4]				<= {(result_sub[3]), mantisse_p2[3][14:12]};
			end
			
			//Step5
			if(result_sub[4] >= (3*{mantisse_result_p[4][8:5],1'b0}*(1+{mantisse_result_p[4][8:5],1'b0})+1'b1)) begin
				mantisse_result_p[5][4]		<= 1'b1;
				result_sub[5]				<= { result_sub[4]-(3*{mantisse_result_p[4][8:5],1'b0}*(1+{mantisse_result_p[4][8:5],1'b0})+1'b1), mantisse_p2[4][11:9]};
			end
			
			else begin
				mantisse_result_p[5][4]		<= 1'b0;
				result_sub[5]				<= {(result_sub[4]), mantisse_p2[4][11:9]};
			end
			
			//Step6
			if(result_sub[5] >= (3*{mantisse_result_p[5][8:4],1'b0}*(1+{mantisse_result_p[5][8:4],1'b0})+1'b1)) begin
				mantisse_result_p[6][3]		<= 1'b1;
				result_sub[6]				<= { result_sub[5]-(3*{mantisse_result_p[5][8:4],1'b0}*(1+{mantisse_result_p[5][8:4],1'b0})+1'b1), mantisse_p2[5][8:6]};
			end
			
			else begin
				mantisse_result_p[6][3]		<= 1'b0;
				result_sub[6]				<= {(result_sub[5]), mantisse_p2[5][8:6]};
			end
			
			//Step7
			if(result_sub[6] >= (3*{mantisse_result_p[6][8:3],1'b0}*(1+{mantisse_result_p[6][8:3],1'b0})+1'b1)) begin
				mantisse_result_p[7][2]		<= 1'b1;
				result_sub[7]				<= { result_sub[6]-(3*{mantisse_result_p[6][8:3],1'b0}*(1+{mantisse_result_p[6][8:3],1'b0})+1'b1), mantisse_p2[6][5:3]};
			end
			
			else begin
				mantisse_result_p[7][2]		<= 1'b0;
				result_sub[7]				<= {(result_sub[6]), mantisse_p2[6][5:3]};
			end
			
			//Step8
			if(result_sub[7] >= (3*{mantisse_result_p[7][8:2],1'b0}*(1+{mantisse_result_p[7][8:2],1'b0})+1'b1)) begin
				mantisse_result_p[8][1]		<= 1'b1;
				result_sub[8]				<= { result_sub[7]-(3*{mantisse_result_p[7][8:2],1'b0}*(1+{mantisse_result_p[7][8:2],1'b0})+1'b1), mantisse_p2[7][2:0]};
			end
			
			else begin
				mantisse_result_p[8][1]		<= 1'b0;
				result_sub[8]				<= {(result_sub[7]), mantisse_p2[7][2:0]};
			end
			
			//Step9
			if(result_sub[8] >= (3*{mantisse_result_p[8][8:1],1'b0}*(1+{mantisse_result_p[8][8:1],1'b0})+1'b1)) begin
				mantisse_result_p[9][0]		<= 1'b1;
				result_sub[9]				<= { result_sub[8]-(3*{mantisse_result_p[8][8:1],1'b0}*(1+{mantisse_result_p[8][8:1],1'b0})+1'b1), 3'b0};
			end
			
			else begin
				mantisse_result_p[9][0]		<= 1'b0;
				result_sub[9]				<= {(result_sub[8]), 3'b0};
			end
		
		end
	end
	
	
	/////////////////////////
	// trans data - 9 Steps
	/////////////////////////
	always @(posedge clk) begin
        if ((~reset_n) | spl_reset) begin
			data_out			<= 32'b0;
			tx_data_rich_valid  <=  1'b0;
        end
        
        else begin
			data_out			<= {sign_p2[cqrt_pipe-1], exponent_new_p[cqrt_pipe-1], mantisse_result_p[cqrt_pipe-1][7:0], 15'b0};
			tx_data_rich_valid  <= dataValid_p2[cqrt_pipe-1];
		end
	end
*/	

	////////////////////////
	// cubic root - 23-Steps
	////////////////////////
	always @(posedge clk) begin
        if ((~reset_n) | spl_reset) begin
		
			for(i=0;i<cqrt_pipe;i++) begin
				mantisse_p2[i]				<= 72'b0;
				mantisse_p2_tmp[i]			<= 72'b0;
				//modulo_p2[i]				<=  2'b0;
				mantisse_result_p[i]		<= 24'b0;
				mantisse_result_p_tmp[i]	<= 24'b0;
				result_sub[i]				<= 72'b0;
				result_sub_tmp[i]			<= 72'b0;
				calcMult[i]					<= 72'b0;
			end		
			
			for(i=0;i<2*cqrt_pipe;i++) begin
				exponent_new_p[i]			<=  8'b0;
				sign_p2[i]					<=  1'b0;
				dataValid_p2[i]				<=  1'b0;			
			end

        end
        
        else begin
		
			mantisse_p2[0]					<= {mantisse_prepared,45'b0};
			mantisse_p2_tmp[0]				<= mantisse_p2[0];
			//modulo_p2[0]					<= modulo_p;
			mantisse_result_p[0]			<= 24'b0; 
			mantisse_result_p_tmp[0]		<= mantisse_result_p[0];
			result_sub[0]					<= 72'b0;
			result_sub_tmp[0]				<= result_sub[0];
			for(i=1;i<cqrt_pipe;i++) begin
				mantisse_p2[i]				<= mantisse_p2_tmp[i-1];
				mantisse_p2_tmp[i]			<= mantisse_p2[i];
				//modulo_p2[i]				<= modulo_p2[i-1];
				mantisse_result_p[i]		<= mantisse_result_p_tmp[i-1];
				mantisse_result_p_tmp[i]	<= mantisse_result_p[i];
				result_sub[i]				<= result_sub_tmp[i-1];
				result_sub_tmp[i]			<= result_sub[i];
			end
			 
			exponent_new_p[0]				<= exponent_new;
			sign_p2[0]						<= sign_p[3];
			dataValid_p2[0]					<= dataValid_p[3];			
			for(i=1;i<2*cqrt_pipe;i++) begin
				exponent_new_p[i]			<= exponent_new_p[i-1];
				sign_p2[i]					<= sign_p2[i-1];
				dataValid_p2[i]				<= dataValid_p2[i-1];			
			end
			
			
			//Step1
			if(mantisse_p2_tmp[0][71:69] >= 1) begin
				mantisse_result_p[1][23]	<= 1'b1;
				result_sub[1]				<= {66'b0, mantisse_p2_tmp[0][71:69]-1, mantisse_p2_tmp[0][68:66]};
			end
			
			else begin
				mantisse_result_p[1][23]	<= 1'b0;
				result_sub[1]				<= {66'b0, mantisse_p2_tmp[0][71:69], mantisse_p2_tmp[0][68:66]};
			end
			
			
			//Step2_tmp
			calcMult[2]						<= ({mantisse_result_p[1][23],1'b0}*(1+{mantisse_result_p[1][23],1'b0}));
			
			//Step 2
			if(result_sub_tmp[1] >= (3*calcMult[2]+1'b1)) begin
				mantisse_result_p[2][22]	<= 1'b1;
				result_sub[2]				<= { result_sub_tmp[1]-(3*calcMult[2]+1'b1), mantisse_p2_tmp[1][65:63]};
			end
			
			else begin
				mantisse_result_p[2][22]	<= 1'b0;
				result_sub[2]				<= {(result_sub_tmp[1]), mantisse_p2_tmp[1][65:63]};
			end
			
			
			//Step3_tmp
			calcMult[3]						<= {mantisse_result_p[2][23:22],1'b0}*(1+{mantisse_result_p[2][23:22],1'b0});

			//Step3
			if(result_sub_tmp[2] >= (3*calcMult[3]+1'b1)) begin
				mantisse_result_p[3][21]	<= 1'b1;
				result_sub[3]				<= { result_sub_tmp[2]-(3*calcMult[3]+1'b1), mantisse_p2_tmp[2][62:60]};
			end
			
			else begin
				mantisse_result_p[3][21]	<= 1'b0;
				result_sub[3]				<= {(result_sub_tmp[2]), mantisse_p2_tmp[2][62:60]};
			end
			
			
			//Step4_tmp
			calcMult[4]						<= {mantisse_result_p[3][23:21],1'b0}*(1+{mantisse_result_p[3][23:21],1'b0});
			
			//Step4
			if(result_sub_tmp[3] >= (3*calcMult[4]+1'b1)) begin
				mantisse_result_p[4][20]	<= 1'b1;
				result_sub[4]				<= { result_sub_tmp[3]-(3*calcMult[4]+1'b1), mantisse_p2_tmp[3][59:57]};
			end
			
			else begin
				mantisse_result_p[4][20]	<= 1'b0;
				result_sub[4]				<= {(result_sub_tmp[3]), mantisse_p2_tmp[3][59:57]};
			end
			
			
			//Step5_tmp
			calcMult[5]						<= {mantisse_result_p[4][23:20],1'b0}*(1+{mantisse_result_p[4][23:20],1'b0});
			
			//Step5
			if(result_sub_tmp[4] >= (3*calcMult[5]+1'b1)) begin
				mantisse_result_p[5][19]	<= 1'b1;
				result_sub[5]				<= { result_sub_tmp[4]-(3*calcMult[5]+1'b1), mantisse_p2_tmp[4][56:54]};
			end
			
			else begin
				mantisse_result_p[5][19]	<= 1'b0;
				result_sub[5]				<= {(result_sub_tmp[4]), mantisse_p2_tmp[4][56:54]};
			end
			

			//Step6_tmp
			calcMult[6]						<= {mantisse_result_p[5][23:19],1'b0}*(1+{mantisse_result_p[5][23:19],1'b0});
			
			//Step6
			if(result_sub_tmp[5] >= (3*calcMult[6]+1'b1)) begin
				mantisse_result_p[6][18]	<= 1'b1;
				result_sub[6]				<= { result_sub_tmp[5]-(3*calcMult[6]+1'b1), mantisse_p2_tmp[5][53:51]};
			end
			
			else begin
				mantisse_result_p[6][18]	<= 1'b0;
				result_sub[6]				<= {(result_sub_tmp[5]), mantisse_p2_tmp[5][53:51]};
			end
			
			
			//Step7_tmp
			calcMult[7]						<= {mantisse_result_p[6][23:18],1'b0}*(1+{mantisse_result_p[6][23:18],1'b0});
			
			//Step7
			if(result_sub_tmp[6] >= (3*calcMult[7]+1'b1)) begin
				mantisse_result_p[7][17]	<= 1'b1;
				result_sub[7]				<= { result_sub_tmp[6]-(3*calcMult[7]+1'b1), mantisse_p2_tmp[6][50:48]};
			end
			
			else begin
				mantisse_result_p[7][17]	<= 1'b0;
				result_sub[7]				<= {(result_sub_tmp[6]), mantisse_p2_tmp[6][50:48]};
			end
			
			
			//Step8_tmp
			calcMult[8]						<= {mantisse_result_p[7][23:17],1'b0}*(1+{mantisse_result_p[7][23:17],1'b0});
			
			//Step8
			if(result_sub_tmp[7] >= (3*calcMult[8]+1'b1)) begin
				mantisse_result_p[8][16]	<= 1'b1;
				result_sub[8]				<= { result_sub_tmp[7]-(3*calcMult[8]+1'b1), mantisse_p2_tmp[7][47:45]};
			end
			
			else begin
				mantisse_result_p[8][16]	<= 1'b0;
				result_sub[8]				<= {(result_sub_tmp[7]), mantisse_p2_tmp[7][47:45]};
			end
			
			
			//Step9_tmp
			calcMult[9]						<= {mantisse_result_p[8][23:16],1'b0}*(1+{mantisse_result_p[8][23:16],1'b0});
			
			//Step9
			if(result_sub_tmp[8] >= (3*calcMult[9]+1'b1)) begin
				mantisse_result_p[9][15]	<= 1'b1;
				result_sub[9]				<= { result_sub_tmp[8]-(3*calcMult[9]+1'b1), mantisse_p2_tmp[8][44:42]};
			end
			
			else begin
				mantisse_result_p[9][15]	<= 1'b0;
				result_sub[9]				<= {(result_sub_tmp[8]), mantisse_p2_tmp[8][44:42]};
			end
			

			//Step10_tmp
			calcMult[10]					<= {mantisse_result_p[9][23:15],1'b0}*(1+{mantisse_result_p[9][23:15],1'b0});
			
			//Step10
			if(result_sub_tmp[9] >= (3*calcMult[10]+1'b1)) begin
				mantisse_result_p[10][14]	<= 1'b1;
				result_sub[10]				<= { result_sub_tmp[9]-(3*calcMult[10]+1'b1), mantisse_p2_tmp[9][41:39]};
			end
			
			else begin
				mantisse_result_p[10][14]	<= 1'b0;
				result_sub[10]				<= {(result_sub_tmp[9]), mantisse_p2_tmp[9][41:39]};
			end
			

			//Step11_tmp
			calcMult[11]					<= {mantisse_result_p[10][23:14],1'b0}*(1+{mantisse_result_p[10][23:14],1'b0});
			
			//Step11
			if(result_sub_tmp[10] >= (3*calcMult[11]+1'b1)) begin
				mantisse_result_p[11][13]	<= 1'b1;
				result_sub[11]				<= { result_sub_tmp[10]-(3*calcMult[11]+1'b1), mantisse_p2_tmp[10][38:36]};
			end
			
			else begin
				mantisse_result_p[11][13]	<= 1'b0;
				result_sub[11]				<= {(result_sub_tmp[10]), mantisse_p2_tmp[10][38:36]};
			end

			
			//Step12_tmp
			calcMult[12]					<= {mantisse_result_p[11][23:13],1'b0}*(1+{mantisse_result_p[11][23:13],1'b0});
			
			//Step12
			if(result_sub_tmp[11] >= (3*calcMult[12]+1'b1)) begin
				mantisse_result_p[12][12]	<= 1'b1;
				result_sub[12]				<= { result_sub_tmp[11]-(3*calcMult[12]+1'b1), mantisse_p2_tmp[11][35:33]};
			end
			
			else begin
				mantisse_result_p[12][12]	<= 1'b0;
				result_sub[12]				<= {(result_sub_tmp[11]), mantisse_p2_tmp[11][35:33]};
			end
			

			//Step13_tmp
			calcMult[13]					<= {mantisse_result_p[12][23:12],1'b0}*(1+{mantisse_result_p[12][23:12],1'b0});
			
			//Step13
			if(result_sub_tmp[12] >= (3*calcMult[13]+1'b1)) begin
				mantisse_result_p[13][11]	<= 1'b1;
				result_sub[13]				<= { result_sub_tmp[12]-(3*calcMult[13]+1'b1), mantisse_p2_tmp[12][32:30]};
			end
			
			else begin
				mantisse_result_p[13][11]	<= 1'b0;
				result_sub[13]				<= {(result_sub_tmp[12]), mantisse_p2_tmp[12][32:30]};
			end
			

			//Step14_tmp
			calcMult[14]					<= {mantisse_result_p[13][23:11],1'b0}*(1+{mantisse_result_p[13][23:11],1'b0});
			
			//Step14
			if(result_sub_tmp[13] >= (3*calcMult[14]+1'b1)) begin
				mantisse_result_p[14][10]	<= 1'b1;
				result_sub[14]				<= { result_sub_tmp[13]-(3*calcMult[14]+1'b1), mantisse_p2_tmp[13][29:27]};
			end
			
			else begin
				mantisse_result_p[14][10]	<= 1'b0;
				result_sub[14]				<= {(result_sub_tmp[13]), mantisse_p2_tmp[13][29:27]};
			end
			

			//Step15_tmp
			calcMult[15]					<= {mantisse_result_p[14][23:10],1'b0}*(1+{mantisse_result_p[14][23:10],1'b0});
			
			//Step15
			if(result_sub_tmp[14] >= (3*calcMult[15]+1'b1)) begin
				mantisse_result_p[15][9]	<= 1'b1;
				result_sub[15]				<= { result_sub_tmp[14]-(3*calcMult[15]+1'b1), mantisse_p2_tmp[14][26:24]};
			end
			
			else begin
				mantisse_result_p[15][9]	<= 1'b0;
				result_sub[15]				<= {(result_sub_tmp[14]), mantisse_p2_tmp[14][26:24]};
			end
			

			//Step16_tmp
			calcMult[16]					<= {mantisse_result_p[15][23:9],1'b0}*(1+{mantisse_result_p[15][23:9],1'b0});
			
			//Step16
			if(result_sub_tmp[15] >= (3*calcMult[16]+1'b1)) begin
				mantisse_result_p[16][8]	<= 1'b1;
				result_sub[16]				<= { result_sub_tmp[15]-(3*calcMult[16]+1'b1), mantisse_p2_tmp[15][23:21]};
			end
			
			else begin
				mantisse_result_p[16][8]	<= 1'b0;
				result_sub[16]				<= {(result_sub_tmp[15]), mantisse_p2_tmp[15][23:21]};
			end
			

			//Step17_tmp
			calcMult[17]					<= {mantisse_result_p[16][23:8],1'b0}*(1+{mantisse_result_p[16][23:8],1'b0});
			
			//Step17
			if(result_sub_tmp[16] >= (3*calcMult[17]+1'b1)) begin
				mantisse_result_p[17][7]	<= 1'b1;
				result_sub[17]				<= { result_sub_tmp[16]-(3*calcMult[17]+1'b1), mantisse_p2_tmp[6][20:18]};
			end
			
			else begin
				mantisse_result_p[17][7]	<= 1'b0;
				result_sub[17]				<= {(result_sub_tmp[16]), mantisse_p2_tmp[16][20:18]};
			end
			

			//Step18_tmp
			calcMult[18]					<= {mantisse_result_p[17][23:7],1'b0}*(1+{mantisse_result_p[17][23:7],1'b0});
			
			//Step18
			if(result_sub_tmp[17] >= (3*calcMult[18]+1'b1)) begin
				mantisse_result_p[18][6]	<= 1'b1;
				result_sub[18]				<= { result_sub_tmp[7]-(3*calcMult[18]+1'b1), mantisse_p2_tmp[17][17:15]};
			end
			
			else begin
				mantisse_result_p[18][6]	<= 1'b0;
				result_sub[18]				<= {(result_sub_tmp[17]), mantisse_p2_tmp[17][17:15]};
			end
			

			//Step19_tmp
			calcMult[19]					<= {mantisse_result_p[18][23:6],1'b0}*(1+{mantisse_result_p[18][23:6],1'b0});
			
			//Step19
			if(result_sub_tmp[18] >= (3*calcMult[19]+1'b1)) begin
				mantisse_result_p[19][5]	<= 1'b1;
				result_sub[19]				<= { result_sub_tmp[18]-(3*calcMult[19]+1'b1), mantisse_p2_tmp[18][14:12]};
			end
			
			else begin
				mantisse_result_p[19][5]	<= 1'b0;
				result_sub[19]				<= {(result_sub_tmp[18]), mantisse_p2_tmp[18][14:12]};
			end
			

			//Step20_tmp
			calcMult[20]					<= {mantisse_result_p[19][23:5],1'b0}*(1+{mantisse_result_p[19][23:5],1'b0});
			
			//Step20
			if(result_sub_tmp[19] >= (3*calcMult[20]+1'b1)) begin
				mantisse_result_p[20][4]	<= 1'b1;
				result_sub[20]				<= { result_sub_tmp[19]-(3*calcMult[20]+1'b1), mantisse_p2_tmp[19][11:9]};
			end
			
			else begin
				mantisse_result_p[20][4]	<= 1'b0;
				result_sub[20]				<= {(result_sub_tmp[19]), mantisse_p2_tmp[19][11:9]};
			end
			

			//Step21_tmp
			calcMult[21]					<= {mantisse_result_p[20][23:4],1'b0}*(1+{mantisse_result_p[20][23:4],1'b0});
			
			//Step21
			if(result_sub_tmp[20] >= (3*calcMult[21]+1'b1)) begin
				mantisse_result_p[21][3]	<= 1'b1;
				result_sub[21]				<= { result_sub_tmp[20]-(3*calcMult[21]+1'b1), mantisse_p2_tmp[20][8:6]};
			end
			
			else begin
				mantisse_result_p[21][3]	<= 1'b0;
				result_sub[21]				<= {(result_sub_tmp[20]), mantisse_p2_tmp[20][8:6]};
			end


			//Step22_tmp
			calcMult[22]					<= {mantisse_result_p[21][23:3],1'b0}*(1+{mantisse_result_p[21][23:3],1'b0});
			
			//Step22
			if(result_sub_tmp[21] >= (3*calcMult[22]+1'b1)) begin
				mantisse_result_p[22][2]	<= 1'b1;
				result_sub[22]				<= { result_sub_tmp[21]-(3*calcMult[22]+1'b1), mantisse_p2_tmp[21][5:3]};
			end
			
			else begin
				mantisse_result_p[22][2]	<= 1'b0;
				result_sub[22]				<= {(result_sub_tmp[21]), mantisse_p2_tmp[21][5:3]};
			end
			

			//Step23_tmp
			calcMult[23]					<= {mantisse_result_p[22][23:2],1'b0}*(1+{mantisse_result_p[22][23:2],1'b0});
			
			//Step23
			if(result_sub_tmp[22] >= (3*calcMult[23]+1'b1)) begin
				mantisse_result_p[23][1]	<= 1'b1;
				result_sub[23]				<= { result_sub_tmp[22]-(3*calcMult[23]+1'b1), mantisse_p2_tmp[22][2:0]};
			end
			
			else begin
				mantisse_result_p[23][1]	<= 1'b0;
				result_sub[23]				<= {(result_sub_tmp[22]), mantisse_p2_tmp[22][2:0]};
			end
			

			//Step24_tmp
			calcMult[24]					<= {mantisse_result_p[23][23:1],1'b0}*(1+{mantisse_result_p[23][23:1],1'b0});
			
			//Step24
			if(result_sub_tmp[23] >= (3*calcMult[24]+1'b1)) begin
				mantisse_result_p[24][0]	<= 1'b1;
				result_sub[24]				<= { result_sub_tmp[23]-(3*calcMult[24]+1'b1), 3'b0};
			end
			
			else begin
				mantisse_result_p[24][0]	<= 1'b0;
				result_sub[24]				<= {(result_sub_tmp[23]), 3'b0};
			end
			

		
		end
	end


	/////////////////////////
	// trans data - 23 Steps
	/////////////////////////
	always @(posedge clk) begin
        if ((~reset_n) | spl_reset) begin
			data_out			<= 32'b0;
			tx_data_rich_valid  <=  1'b0;
        end
        
        else begin
			data_out			<= {sign_p2[2*cqrt_pipe-1-1], exponent_new_p[2*cqrt_pipe-1-1], mantisse_result_p[24][22:0]};
			tx_data_rich_valid  <= dataValid_p2[2*cqrt_pipe-1-1];
		end
	end

endmodule        
