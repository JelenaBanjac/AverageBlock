module stim_gen_file (
   // Input
   clk,  
   // Outputs
   data
   );
   
   input clk;
   output [31:0] data;
   reg    clk;
   reg [31:0] data;
   real data_tmp;
   integer   fd;
   integer   code, dummy;
   reg [32*10:1] str;
   
   initial begin
      fd = $fopen("fileStimuli/input.dat","r"); 
      data = 0;
	  data_tmp = 0;
      code = 1;
      //$monitor("data = %x", data);
      while (code) begin
         code = $fgets(str, fd);
         dummy = $sscanf(str, "%f", data_tmp);
		 data =  $shortrealtobits(data_tmp*data_tmp*data_tmp);
		 
         @(posedge clk);
      end
      $finish;
   end // initial begin
endmodule // stim_gen