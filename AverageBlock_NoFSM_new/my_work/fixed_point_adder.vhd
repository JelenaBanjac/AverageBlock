library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_signed.all;

entity fixed_point_adder is
	generic(
		N    : positive := 1;
		RST_INIT : integer := 0
	);
	port(
		-- Clock.
		i_clk  	: in  std_logic;
		-- Reset.
		i_rst 	: in  std_logic;
		
		-- Input port.
		i_A    	: in  std_logic_vector(N-1 downto 0);
		i_B    	: in  std_logic_vector(N-1 downto 0);
		
		-- Output stuff.
		o_C    	: out std_logic_vector(N-1 downto 0)
	);
end entity fixed_point_adder;

architecture arch_fixed_point_adder of fixed_point_adder is
	
	signal A, B, C : std_logic_vector(N-1 downto 0);
	
begin
	
	A <= i_A;
	B <= i_B;
	
	process(A, B)
	begin
		if i_rst = '0' then
			C <= conv_std_logic_vector(RST_INIT, N);
		else
			if (A(N-1)=B(N-1)) then -- same signs
				C(N-2 downto 0) <= A(N-2 downto 0) + B(N-2 downto 0);	-- default addition
				C(N-1) <= A(N-1);
			else								-- different signs
				if (A(N-1)='0' and B(N-1)='1') then -- r1 is positive and r2 is negative
					if (A(N-2 downto 0) > B(N-2 downto 0)) then	-- r1 is greater than r2
						C(N-2 downto 0) <= A(N-2 downto 0) - B(N-2 downto 0);	-- r1-r2
						C(N-1) <= '0';	-- positive number + (sign from r1)
					else
						C(N-2 downto 0) <= B(N-2 downto 0) - A(N-2 downto 0);	-- r2-r1
						if (C(N-2 downto 0)=0) then
							C(N-1) <= '0';	-- not a negative zero
						else
							C(N-1) <= '1';	-- r2 is negative, r2 is greater than r1, r_temp1 must be negative
						end if;
					end if;
				else	-- r1 is negative and r2 is positive
					if (A(N-2 downto 0) > B(N-2 downto 0)) then	-- r1 > r2
						C(N-2 downto 0) <= A(N-2 downto 0) - B(N-2 downto 0);	-- r1-r2
						if (C(N-2 downto 0)=0) then
							C(N-1) <= '0';	-- not a negative zero
						else
							C(N-1) <= '1';	-- r2 is negative, r2 is greater than r1, r_temp1 must be negative
						end if;
					else -- r2 >= r1
						C(N-2 downto 0) <= B(N-2 downto 0) - A(N-2 downto 0);	-- r2-r1
						C(N-1) <= '0';	
					end if;
				end if;				
			end if;
		end if;
	end process;
	
	o_C <= C;
	
end architecture arch_fixed_point_adder;
