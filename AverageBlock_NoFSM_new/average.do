##################################
# A very simple modelsim do file #
##################################

# 1) Create a library for working in
#vlib work
vlib mti_lib

#vmap work work

# 2) Compile the average
#vcom average_tb.vhd
vcom work/fixed_point_adder.vhd
vlog work/qdiv.v
vcom work/average.vhd
vcom average_tb.vhd


# 3) Load it for simulation
vsim -novopt average_tb


# 4) Open some selected windows for viewing
view structure
view signals
view wave

# 5) Show some of the signals in the wave window
add wave -noupdate -divider -height 32 Inputs
add wave -noupdate a_clk
add wave -noupdate a_rst
add wave -noupdate -dec available
add wave -noupdate data_valid
add wave -noupdate -dec data_in_X
add wave -noupdate -dec data_in_Y
add wave -noupdate -dec data_in_Z
add wave -noupdate -divider -height 32 Outputs
add wave -noupdate -dec data_out_addition
add wave -noupdate -dec data_valid_cnf_addition
add wave -noupdate -dec data_out
add wave -noupdate -dec data_valid_cnf
#add wave -noupdate -dec urOutput

# 6) Set some test patterns
run 1000 ns

wave zoomfull
